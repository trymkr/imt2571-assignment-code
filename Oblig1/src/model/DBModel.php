<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            $this->db = new PDO('mysql:dbname=test' . ';host=localhost',
            "root", "",
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
    }

    protected function ValidateBook($book)
    {
        if ($book == null)
        {
            throw new PDOException("Book is null!");
        }
        else if($book->title == null || $book->author == null)
        {
            throw new PDOException("Book title or author is empty!");
        }

        return true;
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();

        foreach($this->db->query('SELECT * FROM Book') as $row)
        {
            array_push($booklist, new Book( $row['title'], 
                                            $row['author'], 
                                            $row['description'],
                                            $row['id']));
        }

        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {

        if (!is_numeric($id))
        {
            return null;
        }

        $book = null;
        
        $stmt = $this->db->prepare('SELECT * FROM book WHERE id=?');
        $stmt->bindValue(1, $id);
        $stmt->execute();

        // Since the ID is unique we can do the assumption of only 1 row returned
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // If a row was returned set the book object, if not we throw an exception
        if ($row != null)
        {
            $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }


        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        $this->ValidateBook($book);

        $stmt = $this->db->prepare('INSERT INTO book(title, author, description) VALUES(:title, :author, :description)');
        $stmt->bindValue(':title', $book->title);
        $stmt->bindValue(':author', $book->author);
        $stmt->bindValue(':description', $book->description);
        $stmt->execute();

        $book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if ($book == null)
        {
            throw new PDOException("Trying to modify a null book!");
        }

        $this->ValidateBook($book);

        if ($this->getBookById($book->id))
        {
            $stmt = $this->db->prepare( 'UPDATE Book SET title=?, author=?, description=? WHERE id=?');
            
            $stmt->bindValue(1, $book->title);
            $stmt->bindValue(2, $book->author);
            $stmt->bindValue(3, $book->description);
            $stmt->bindValue(4, $book->id);

            $stmt->execute();
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if ($this->getBookById($id))
        {
            $stmt = $this->db->prepare('DELETE FROM book WHERE id=?');
            $stmt->bindValue(1, $id);
            $stmt->execute();
        }
    }
	
}

?>